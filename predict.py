import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import holidays
import statsmodels.formula.api as smf

data = pd.read_csv('Helsingin_pyorailijamaarat.sane.csv', parse_dates=["Päivämäärä"])
data.rename(columns={"Päivämäärä": "date"}, inplace=True)
data = data[data.date.notnull()]
del data["Unnamed: 21"]
data.set_index(data.date)
data = data.melt(id_vars="date", var_name="location", value_name="cyclists")

weather = pd.read_csv('kaisaniemi_weather.csv')
weather.rename(inplace=True, columns={"Vuosi": "year", "Kk": "month", "Pv": "day",
    "Ilman lämpötila (degC)": "temperature",
    "Sademäärä (mm)": "rainfall",
    "Lumensyvyys (cm)": "snow"})
weather["date"] = pd.to_datetime(weather[['year', 'month', 'day']]) + pd.Timedelta(days=0)
weather["dayofyear"] = weather.date.dt.dayofyear
weather["rainfall"][weather.rainfall < 0] = 0.0
weather["snow"][weather.snow.isnull()] = 0.0

mean_weather = weather.groupby(weather.date.dt.dayofyear).mean()

#plt.plot(mean_weather.temperature)
#plt.show()

# https://stackoverflow.com/questions/6372802/calculate-daylight-hours-based-on-gegraphical-coordinates
def daylight(latitude,day):
    P = np.arcsin(0.39795 * np.cos(0.2163108 + 2 * np.arctan(0.9671396 * np.tan(.00860 * (day - 186)))))
    pi = np.pi
    daylightamount = 24 - (24 / pi) * np.arccos(
        (np.sin((0.8333 * pi / 180) + np.sin(latitude * pi / 180) * np.sin(P)) / (np.cos(latitude * pi / 180) * np.cos(P))))
    return daylightamount

fin_holidays = holidays.FI()
"""
for l, ld in data.groupby("location"):
    plt.title(l)
    daily = ld.groupby(ld.date.dt.floor("d")).sum()
    holidays = daily[daily.index.map(lambda x: x in fin_holidays)]
    daily = daily[daily.index.map(lambda x: x not in fin_holidays)]
    daily = daily[daily.index.dayofweek < 5]
    plt.plot((daily.cyclists))
    plt.plot((holidays), '.')
    plt.show()
"""
baana = data[data.location=="Baana"]
baana = baana.groupby(baana.date.dt.floor("d")).sum().reset_index()
baana = baana.merge(weather, on='date')
baana["dayofyear"] = baana.date.dt.dayofyear
baana = baana.merge(mean_weather, how="left", on='dayofyear', suffixes=("", "_mean"))
baana["logcyclists"] = np.log1p(baana.cyclists)
baana["is_holiday"] = (baana.date.dt.dayofweek >= 5) | baana.date.map(lambda x: x in fin_holidays)
baana["is_july"] = (baana.date.dt.month == 7)
baana["daylight"] = daylight(24.9384, baana.date.dt.dayofyear + 1)
baana["dayno"] = (baana.date - pd.to_datetime("2015-1-1")).dt.days/365
model = smf.ols("logcyclists ~ I(np.arctan((temperature - 5)*0.25)) + is_holiday + rainfall + snow + is_july + dayno", data=baana).fit()
print(model.summary())

#plt.plot(baana["temperature"][~baana.is_holiday], color='green')
#plt.twinx()
plt.plot(baana.cyclists)
plt.plot(np.exp(model.predict()) - 1)
plt.figure()

def sigmoid(s, x0, k):
    #return lambda x: s*(np.log(1.0/(1 + np.exp(-(x - x0)))) + k)
    return lambda x: s*np.arctan((x - x0)*k)

from scipy.optimize import least_squares

residual = (baana.logcyclists - model.predict()).values
plt.plot(residual)
plt.figure()
def error(params):
    return sigmoid(*params)(baana.temperature.values) - residual
fit = least_squares(error, (1, 0, 1), loss="soft_l1")
print(fit)

rng = np.linspace(baana.cyclists.min(), baana.cyclists.max())
plt.plot(baana.cyclists, np.exp(model.predict()) - 1, '.')
plt.plot(rng, rng)
plt.xlabel("Cyclists/day (actual)")
plt.ylabel("Cyclists/day (predicted)")
plt.show()

#weather = pd.read_csv('kaisaniemi_weather.csv')
#weather.rename(inplace=True, columns={"Vuosi": "year", "Kk": "month", "Pv": "day"})
#weather["date"] = pd.to_datetime(weather[['year', 'month', 'day']]) + pd.Timedelta(days=0)
